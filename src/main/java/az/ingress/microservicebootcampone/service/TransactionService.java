package az.ingress.microservicebootcampone.service;

import az.ingress.microservicebootcampone.entity.Account;
import az.ingress.microservicebootcampone.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;


import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransactionService {

    private final AccountRepository repository;

    @SneakyThrows
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void transfer(Account from,Account to,double balance){
        log.info("transfer start!!");

        if(from.getBalance() < balance){
            throw new RuntimeException("not sufficient balance");
        }

        log.info("sleep 5 sec.");
        Thread.sleep(5000);


        from.setBalance(from.getBalance() - balance);


        to.setBalance(to.getBalance() + balance);

        repository.save(from);
//
//        if(true){
//            throw new RuntimeException();
//        }

        repository.save(to);


    }
}
