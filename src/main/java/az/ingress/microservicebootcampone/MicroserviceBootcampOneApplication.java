package az.ingress.microservicebootcampone;

import az.ingress.microservicebootcampone.entity.*;
import az.ingress.microservicebootcampone.repository.AccountRepository;
import az.ingress.microservicebootcampone.repository.AddressRepository;
import az.ingress.microservicebootcampone.repository.PersonRepository;
import az.ingress.microservicebootcampone.repository.PhoneNumberRepository;
import az.ingress.microservicebootcampone.service.TransactionService;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;


@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
public class MicroserviceBootcampOneApplication implements CommandLineRunner {

    private final PersonRepository personRepository;
    private final AddressRepository addressRepository;
    private final PhoneNumberRepository phoneNumberRepository;
    private final JPAStreamer jpaStreamer;
    private final TransactionService transactionService;
    private final AccountRepository accountRepository;

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceBootcampOneApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        Account ismayil = accountRepository.findById(1L).get();
//        Account eli = accountRepository.findById(2L).get();


//        transactionService.transfer(ismayil,eli,200);



    }
}
