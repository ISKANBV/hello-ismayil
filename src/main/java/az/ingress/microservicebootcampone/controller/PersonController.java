package az.ingress.microservicebootcampone.controller;

import az.ingress.microservicebootcampone.entity.Account;
import az.ingress.microservicebootcampone.entity.GenericSpecification;
import az.ingress.microservicebootcampone.entity.Person;
import az.ingress.microservicebootcampone.entity.SearchCriteria;
import az.ingress.microservicebootcampone.repository.AccountRepository;
import az.ingress.microservicebootcampone.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/person")
public class PersonController {

    private final PersonRepository repository;
    private final AccountRepository accountRepository;

    public void add(@RequestBody Person person){
        repository.save(person);
    }

    @GetMapping("/")
    public List<Person> getAll(){
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public Account getById(@PathVariable(name = "id") long id){
        return accountRepository.findById(id).get();
    }

    @GetMapping("/criteria")
    public List<Account> getAccountByCriteria(@RequestBody List<SearchCriteria> criteria) {
        GenericSpecification<Account> a = new GenericSpecification<>();
        a.add(criteria);
        return accountRepository.findAll(a);
    }
}
