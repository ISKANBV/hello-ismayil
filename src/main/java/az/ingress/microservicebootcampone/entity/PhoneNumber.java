package az.ingress.microservicebootcampone.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "phone_number")
public class PhoneNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String phone;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.PERSIST)
    @ToString.Exclude
    @JoinColumn(name = "person_id")
    Person person;
}
