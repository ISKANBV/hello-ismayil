package az.ingress.microservicebootcampone.repository;

import az.ingress.microservicebootcampone.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Long> {
}
