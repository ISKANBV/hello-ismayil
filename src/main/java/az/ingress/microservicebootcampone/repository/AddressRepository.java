package az.ingress.microservicebootcampone.repository;

import az.ingress.microservicebootcampone.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Long> {
}
