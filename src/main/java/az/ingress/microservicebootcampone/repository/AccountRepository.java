package az.ingress.microservicebootcampone.repository;

import az.ingress.microservicebootcampone.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AccountRepository extends JpaRepository<Account,Long> , JpaSpecificationExecutor<Account> {
}
