package az.ingress.microservicebootcampone.repository;

import az.ingress.microservicebootcampone.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person,Long> {
}
